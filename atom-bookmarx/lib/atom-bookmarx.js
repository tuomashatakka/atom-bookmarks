'use babel';

import AtomBookmarxView from './atom-bookmarx-view';
import { CompositeDisposable } from 'atom';

export default {

  atomBookmarxView: null,
  modalPanel: null,
  subscriptions: null,

  activate(state) {
    this.atomBookmarxView = new AtomBookmarxView(state.atomBookmarxViewState);
    this.modalPanel = atom.workspace.addModalPanel({
      item: this.atomBookmarxView.getElement(),
      visible: false
    });

    // Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    this.subscriptions = new CompositeDisposable();

    // Register command that toggles this view
    this.subscriptions.add(atom.commands.add('atom-workspace', {
      'atom-bookmarx:toggle': () => this.toggle()
    }));
  },

  deactivate() {
    this.modalPanel.destroy();
    this.subscriptions.dispose();
    this.atomBookmarxView.destroy();
  },

  serialize() {
    return {
      atomBookmarxViewState: this.atomBookmarxView.serialize()
    };
  },

  toggle() {
    console.log('AtomBookmarx was toggled!');
    return (
      this.modalPanel.isVisible() ?
      this.modalPanel.hide() :
      this.modalPanel.show()
    );
  }

};
